package deutschebank.dbutils;

public class Deal {
    private int     itsDealID;
    private String    itsDealTime;
    private String    itsDealType;
    private double  itsDealAmount;
    private int     itsDealQuantity;
    private String  itsInstrumentName;
    private String itsCounterpartyName;
    private String itsCounterpartyStatus;
    private String itsCounterpartyDate;

//    public Deal( int id, String time, String type, double amount, int quantity, String instrumentName, String counterpartyName, String counterpartyStatus, String counterpartyDate ) {
public Deal( int id, String time, String type, double amount, int quantity) {
        itsDealID = id;
        itsDealTime = time;
        itsDealType = type;
        itsDealAmount = amount;
        itsDealQuantity = quantity;
//        itsInstrumentName = instrumentName;
//        itsCounterpartyName = counterpartyName;
//        itsCounterpartyStatus = counterpartyStatus;
//        itsCounterpartyDate = counterpartyDate;
    }

    public int getDealID()
    {
        return itsDealID;
    }

    public void setDealID(int itsDealID)
    {
        this.itsDealID = itsDealID;
    }

    public String getDealTime() {
        return itsDealTime;
    }

    public void setDealTime(String itsDealTime)
    {
        this.itsDealTime = itsDealTime;
    }

    public String getDealType() {
        return itsDealType;
    }

    public void setDealType(String itsDealType)
    {
        this.itsDealType = itsDealType;
    }

    public double getDealAmount()
    {
        return itsDealAmount;
    }

    public void setDealAmount(double itsDealAmount)
    {
        this.itsDealAmount = itsDealAmount;
    }

    public int getDealQuantity() {
        return itsDealQuantity;
    }

    public void setDealQuantity(int itsDealQuantity)
    {
        this.itsDealQuantity = itsDealQuantity;
    }

//    public String getInstrumentName()
//    {
//        return itsInstrumentName;
//    }

//    public void setInstrumentName(String itsInstrumentName)
//    {
//        this.itsInstrumentName = itsInstrumentName;
//    }
//
//    public String getCounterpartyName() {
//        return itsCounterpartyName;
//}
//
//    public String getCounterpartyStatus() {
//        return itsCounterpartyStatus;
//    }
//
//    public String getCounterpartyDate() {
//        return itsCounterpartyDate;
//    }
//
//    public void setCounterpartyName(String itsCounterpartyName) {
//        this.itsCounterpartyName = itsCounterpartyName ;
//    }
//
//    public void setCounterpartyStatus(String itsCounterpartyStatus) { this.itsCounterpartyStatus = itsCounterpartyStatus; }
//
//    public void setCounterpartyDate(String itsCounterpartyDate) {
//        this.itsCounterpartyDate = itsCounterpartyDate;
//    }
}
