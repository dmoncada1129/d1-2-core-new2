package deutschebank.dbutils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import deutschebank.MainUnit;

public class CounterpartyHandler {
    static  private CounterpartyHandler itsSelf = null;

    private CounterpartyHandler(){}

    static  public  CounterpartyHandler  getLoader()
    {
        if( itsSelf == null )
            itsSelf = new CounterpartyHandler();
        return itsSelf;
    }

//    public  Counterparty  loadFromDB( String dbID, Connection theConnection, int key )
//    {
//        Counterparty result = null;
//        try
//        {
//            String sbQuery = "select * from " + dbID + ".counterparty where counterparty_id=?";
//            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
//            stmt.setInt(1, key);
//            ResultSet rs = stmt.executeQuery();
//
//            CounterpartyIterator iter = new CounterpartyIterator(rs);
//
//            while( iter.next() )
//            {
//                result = iter.buildCounterparty();
//                if(MainUnit.debugFlag)
//                    System.out.println( result.getCounterpartyName() + "//" + result.getCounterpartyStatus() + "/" + result.getCounterpartyDate() );
//            }
//        }
//        catch (SQLException ex)
//        {
//            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return result;
//    }
//
//    public  ArrayList<Counterparty>  loadFromDB( String dbID, Connection theConnection, int lowerKey, int upperKey )
//    {
//        ArrayList<Counterparty> result = new ArrayList<Counterparty>();
//        Counterparty theCounterparty = null;
//        try
//        {
//            String sbQuery = "select * from " + dbID + ".counterparty where counterparty_id>=? and counter_id<=?";
//            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
//            stmt.setInt(1, lowerKey);
//            stmt.setInt(2, upperKey);
//            ResultSet rs = stmt.executeQuery();
//
//            CounterpartyIterator iter = new CounterpartyIterator(rs);
//
//            while( iter.next() )
//            {
//                theCounterparty = iter.buildCounterparty();
//                if(MainUnit.debugFlag)
//                    System.out.println( theCounterparty.getCounterpartyName() + "//" + theCounterparty.getCounterpartyStatus() + "//" + theCounterparty.getCounterpartyDate() );
//                result.add(theCounterparty);
//            }
//        }
//        catch (SQLException ex)
//        {
//            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        return result;
//    }
    public  ArrayList<Counterparty>  loadFromDB( Connection theConnection )
    {
        ArrayList<Counterparty> result = new ArrayList<Counterparty>();
        Counterparty theCounterparty = null;
        try
        {
            String sbQuery = "select * from db_grad_cs_1917.counterparty";
            PreparedStatement stmt = theConnection.prepareStatement(sbQuery);
            ResultSet rs = stmt.executeQuery();

            CounterpartyIterator iter = new CounterpartyIterator(rs);

            while( iter.next() )
            {
                theCounterparty = iter.buildCounterparty();
                if(MainUnit.debugFlag)
                    System.out.println( theCounterparty.getCounterpartyName() + "//" + theCounterparty.getCounterpartyStatus() );
                result.add(theCounterparty);
            }
        }
        catch (SQLException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }


//    public  String  toJSON( Counterparty theCounterparty )
//    {
//        String result = "";
//        try
//        {
//            ObjectMapper mapper = new ObjectMapper();
//            result = mapper.writeValueAsString(theCounterparty);
//        }
//        catch (JsonProcessingException ex)
//        {
//            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException ex)
//        {
//            Logger.getLogger(CounterpartyHandler.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return result;
//    }

    public  String  toJSON( ArrayList<Counterparty> theCounterparties )
    {
        String result = "";
        Counterparty[] insArray = new Counterparty[theCounterparties.size()];
        theCounterparties.toArray(insArray);
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            result = mapper.writeValueAsString(insArray);
        }
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(InstrumentHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
}
